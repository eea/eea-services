# eea-services

Services de vidéo conférences et autres pour maintenir les services durant la pandémie du COVID-19.

## Lancement

```bash
sudo docker-compose build riot
sudo docker-compose up -d
```

## Configuration initiale de postgres

```bash
sudo docker-compose exec postgres psql -U postgres
postgres=# CREATE USER synapse WITH PASSWORD 'CHANGE ME';
CREATE ROLE
postgres=# CREATE DATABASE synapse
postgres-#  ENCODING 'UTF8'
postgres-#  LC_COLLATE='C'
postgres-#  LC_CTYPE='C'
postgres-#  template=template0
postgres-#  OWNER synapse;
```

## Ajout d'un utilisateur

```bash
sudo docker-compose exec synapse register_new_matrix_user -u pseudo -p mot_de_passe -c /data/homeserver.yaml http://localhost:8008
```
